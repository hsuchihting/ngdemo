import { Router } from '@angular/router';

import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
  FormArray,
  AbstractControl,
} from '@angular/forms';
import { Injectable } from '@angular/core';
import { BaseViewModel } from 'src/app/base/base.veiw-model';
import {
  AddTestReq,
  AddTestsQu,
  AddTestsQuOptions,
} from 'src/app/model/addPaperModel';

@Injectable({
  providedIn: 'root',
})
export class AddPaperViewModel extends BaseViewModel {
  //*變數
  addPaperList: AddTestReq[] = [];
  testerArr!: any[];
  testDayArr!: any[];
  testHourArr: any[] = [];
  testMinsArr: any[] = [];
  remindTimeArr!: any[];
  testTypeArr: any[] = [];
  test_root: boolean[] = [];
  answer_root: boolean[] = [];
  addTestsQuList: AddTestsQu[] = [];
  addTestsQuOptionsList: AddTestsQuOptions[] = [];
  examTypeArr: any[] = [];
  selectId: number[] = [0];
  radioRequired: boolean = false;
  checkRequired: boolean = false;
  radioAnsLen: boolean = false;
  checkAnsLen: boolean = false;
  testQuType!: string;
  checked: boolean = true;
  addCount: number = 0;
  testSelected: boolean[] = [];
  testPicSelected: boolean[] = [];
  answerSelected: boolean[] = [];
  answerUploadSelected: boolean[] = [];

  //*form 表單
  addPaper: FormGroup = new FormGroup({
    testsType: new FormControl(''),
    testsName: new FormControl(''),
    testDTime: new FormControl(''),
    testHTime: new FormControl(''),
    testMTime: new FormControl(''),
    remindTime: new FormControl(''),
    memo: new FormControl(''),
  });

  addPaperItem!: FormGroup;
  addPaperItemChild!: FormGroup;
  addAnswerItem!: FormGroup;

  get paperControl() {
    const arr = this.addPaperItem.get('papers') as FormArray;
    return arr.controls as FormGroup[];
  }

  get answerControl() {
    return this.addPaperItem.get('papers') as FormArray;
  }

  constructor(
    private formBuilder: FormBuilder,

    private router: Router
  ) {
    super();
  }

  init(): void {
    this.createForm();
    this.addPaperForm();
    this.getData();
  }

  getData() {
    this.testTypeSelect();
    this.testDayArrSelect();
    this.testHourArrSelect();
    this.testMinsArrSelect();
    this.remindTimeArrSelect();
    this.examTypeSelect();
    this.testRoot();
    this.answerRoot();
    this.getDefaultValue();
  }

  //*題目類別
  testTypeSelect() {
    this.testTypeArr = [
      {
        id: 1,
        type: '類別 1',
      },
      {
        id: 2,
        type: '類別 2',
      },
      {
        id: 3,
        type: '類別 3',
      },
      {
        id: 4,
        type: '類別 4',
      },
    ];
  }

  //*題目類型
  examTypeSelect() {
    this.examTypeArr = [
      {
        id: 0,
        type: 'C',
        name: '單選題',
      },
      {
        id: 1,
        type: 'S',
        name: '複選題',
      },
      {
        id: 2,
        type: 'SD',
        name: '簡答題',
      },
      {
        id: 3,
        type: 'FU',
        name: '檔案上傳題',
      },
    ];
  }

  //*題目類型下拉選單
  examSelect(event: any, i: number): any {
    this.selectId[i] = event.id;
    let selectTypeCode = this.examTypeArr[event.id].type;
    this.testQuType = selectTypeCode;
    console.log(selectTypeCode);
  }

  //*測驗日
  testDayArrSelect() {
    this.testDayArr = [
      {
        id: 1,
        name: '1',
      },
      {
        id: 2,
        name: '5',
      },
      {
        id: 3,
        name: '7',
      },
      {
        id: 4,
        name: '14',
      },
    ];
  }

  //*測驗小時
  testHourArrSelect() {
    for (let i = 0; i < 24; i++) {
      let num = '';
      if (i <= 9) {
        num = '0' + i;
      } else {
        num = i.toString();
      }
      this.testHourArr.push({
        id: i + 1,
        name: num,
      });
    }
  }

  //*測驗分鐘
  testMinsArrSelect() {
    for (let i = 0; i < 60; i++) {
      let num = '';
      if (i <= 9) {
        num = '0' + i;
      } else {
        num = i.toString();
      }
      this.testMinsArr.push({
        id: i + 1,
        name: num,
      });
    }
  }

  //*倒數提醒
  remindTimeArrSelect() {
    this.remindTimeArr = [
      {
        id: 1,
        name: '5',
      },
      {
        id: 2,
        name: '10',
      },

      {
        id: 3,
        name: '15',
      },
      {
        id: 4,
        name: '20',
      },
      {
        id: 5,
        name: '25',
      },
      {
        id: 6,
        name: '30',
      },
    ];
  }

  //*建立上方表單
  createForm() {
    this.addPaper = this.formBuilder.group({
      testsType: ['類別 1'],
      testDTime: ['1'],
      testHTime: ['00'],
      testMTime: ['00'],
      remindTime: ['5'],
      testsName: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(100)]),
      ],
      memo: ['', Validators.maxLength(1000)],
    });
  }

  //*試卷下方表單
  addPaperForm() {
    this.addPaperItemChild = this.addPaperFormChild();
    this.addPaperItem = this.formBuilder.group({
      papers: this.formBuilder.array([this.addPaperItemChild]),
    });
    this.twoTestValidators();
    this.twoAnsValidators();
  }

  //*子試卷表單
  addPaperFormChild() {
    this.addAnswerItem = this.answerItem();
    return this.formBuilder.group({
      radioType: ['單選題'],
      checkboxType: ['複選題'],
      shortAns: ['簡答題'],
      uploadFile: ['檔案上傳題'],
      test: ['', Validators.maxLength(1000)],
      testPic: [null],
      answer: this.formBuilder.array([
        this.formBuilder.group({
          answer: ['', Validators.maxLength(100)],
          answerUpload: [''],
        }),
      ]),
    });
  }

  getDefaultValue() {
    const form = this.addPaperItem.get('papers') as FormArray;
    // let formVal = form.at(0).get('radioType')?.value;
    // console.log(formVal);
    form.at(0).patchValue({
      radioType: this.examTypeArr[0].name,
    });
    this.testQuType = this.examTypeArr[0].type;
  }

  //*答案
  answerItem() {
    return this.formBuilder.group({
      answer: ['', Validators.maxLength(100)],
      answerUpload: [''],
    });
  }

  //*上傳檔案
  onFileChange(event: any, i: number) {
    console.log('index:', i);
    const form = this.addPaperItem.get('papers') as FormArray;
    form.at(i).patchValue({
      testPic: event.name,
    });
  }

  //* 上傳答案
  uploadAns(event: any, i: number, j: number) {
    console.log('index:', i);
    const form = this.answerControl.at(i).get('answer') as FormArray;
    form.at(j).patchValue({
      answerUpload: event.name,
    });
  }

  //*上傳錯誤
  updateError(event: any, index: number) {
    const form = this.addPaperItem.get('papers') as FormArray;
    const testPic = form.at(index).get('testPic') as AbstractControl;
    testPic.setErrors(event);
  }

  //*單選答案
  getRadioAnsValue(event: any, radioIndex: number) {
    const form = this.addPaperItem.get('papers') as FormArray;
    this.addTestsQuOptionsList = [];
    for (let i = 0; i < form.length; i++) {
      const answerGroup = form.at(i).get('answer') as FormArray;
      for (let j = 0; j < answerGroup.length; j++) {
        if (radioIndex === j) {
          event.value = 0;
        } else {
          event.value = 1;
        }
        this.addTestsQuOptionsList.push({
          testsQuOptionsDesc: answerGroup.value[j].answer,
          testsQuOptionsImg: answerGroup.value[j].answerUpload,
          isTestsQuAns: event.value,
        });
      }
    }
  }

  //*複選答案
  getCheckAnsValue(event: any, checkboxIndex: number) {
    const form = this.addPaperItem.get('papers') as FormArray;
    this.addTestsQuOptionsList = [];
    for (let i = 0; i < form.length; i++) {
      const answerGroup = form.at(i).get('answer') as FormArray;
      for (let j = 0; j < answerGroup.length; j++) {
        if (checkboxIndex === j) {
          event.value = 0;
        } else {
          event.value = 1;
        }
        this.addTestsQuOptionsList.push({
          testsQuOptionsDesc: answerGroup.value[j].answer,
          testsQuOptionsImg: answerGroup.value[j].answerUpload,
          isTestsQuAns: event.value,
        });
      }
    }
  }

  //*取得 AddTestsQu 的陣列值
  getAddTestsQu() {
    const form = this.addPaperItem.get('papers') as FormArray;
    form.controls.forEach((item) => {
      let type = item.get('radioType')?.value;
      let change = '';
      switch (type) {
        case '單選題':
          change = 'C';
          break;
        case '複選題':
          change = 'S';
          break;
        case '簡答題':
          change = 'SD';
          break;
        case '檔案上傳題':
          change = 'FU';
          break;
        default:
          break;
      }
      this.addTestsQuList.push({
        testsQuType: change,
        testsQuDesc: item.get('test')?.value,
        testsQuImg: item.get('testPic')?.value,
        addTestsQuOptionsList: this.addTestsQuOptionsList,
      });
    });
  }

  //*儲存試卷
  save() {
    console.log(
      'testsType:',
      this.addPaper.value.testsType,
      ' testsName:',
      this.addPaper.value.testsName,
      'testDTime:',
      this.addPaper.value.testDTime,
      'testHTime:',
      this.addPaper.value.testHTime,
      ' testMTime:',
      this.addPaper.value.testMTime,
      'remindTime:',
      this.addPaper.value.remindTime,
      'memo:',
      this.addPaper.value.memo,
      'addTestsQuList:',
      this.addTestsQuList
    );

    this.addTestsQuList = [];
    this.getAddTestsQu();
    this.twoTestValidators();
    this.twoAnsValidators();
    this.examService
      .addTest({
        testsType: this.addPaper.value.testsType,
        testsName: this.addPaper.value.testsName,
        testDTime: this.addPaper.value.testDTime,
        testHTime: this.addPaper.value.testHTime,
        testMTime: this.addPaper.value.testMTime,
        remindTime: this.addPaper.value.remindTime,
        memo: this.addPaper.value.memo,
        addTestsQuList: this.addTestsQuList,
      })
      .subscribe((res) => {
        const returnCode = res.header?.returnCode;
        const errorMsg = '新增失敗，請洽管理人員確認原因或稍後再試';
        const successMsg = '新增成功';
        if (returnCode === 'B9999') {
          alert(errorMsg);
        } else {
          alert(successMsg);
        }
      });
  }

  //*取消試卷
  cancel() {
    const msg = '資料尚未儲存，是否離開新增頁面';
    alert(msg);
    this.router.navigate(['/main/exam/test-paper']);
  }

  //*新增試卷
  addTestPaper() {
    this.selectId.push(0);
    const form = this.addPaperItem.get('papers') as FormArray;
    console.log(form.length);
    const child = this.addPaperFormChild();
    form.push(child);
  }

  //*刪除試卷
  delTestPaper(index: number) {
    const form = this.addPaperItem.get('papers') as FormArray;
    form.removeAt(index);
  }

  //*垃圾桶顯示刪除條件
  testRoot() {
    this.test_root[0] = false;
    for (let i = 1; i < 10; i++) {
      this.test_root[i] = true;
    }
  }

  //*答案顯示刪除條件
  answerRoot() {
    this.answer_root[0] = false;
    for (let j = 1; j < 10; j++) {
      this.answer_root[j] = true;
    }
  }

  //*新增答案
  add(j: number) {
    const ansList = this.answerControl.at(j).get('answer') as FormArray;
    if (ansList.length < 10) {
      const form = this.answerControl.at(j).get('answer') as FormArray;
      const answer = this.answerItem();
      form.push(answer);
    }
    this.addCount++;
    this.radioConfirm();
    this.checkConfirm();
  }

  //*刪除答案
  del(j: number) {
    const form = this.answerControl.at(j).get('answer') as FormArray;
    form.removeAt(j);
    this.addCount--;
  }

  //*題目枚舉
  answerTitle(idx: number): any {
    switch (idx) {
      case 1:
        return 'A';
      case 2:
        return 'B';
      case 3:
        return 'C';
      case 4:
        return 'D';
      case 5:
        return 'E';
      case 6:
        return 'F';
      case 7:
        return 'G';
      case 8:
        return 'H';
      case 9:
        return 'I';
      case 10:
        return 'J';
      default:
        break;
    }
  }

  //*答案正解 radio 驗證
  radioConfirm() {
    this.radioAnsLen = false;
    if (this.addCount < 1) {
      this.radioAnsLen = true;
    }

    this.radioRequired = this.addTestsQuOptionsList.every((item) => {
      return item.isTestsQuAns == '1';
    });
  }

  //*答案正解 checkbox 驗證
  checkConfirm() {
    this.checkAnsLen = false;
    if (this.addCount < 1) {
      this.checkRequired = true;
    }
    this.checkRequired = this.addTestsQuOptionsList.every((item) => {
      return item.isTestsQuAns == '1';
    });
  }

  //*二擇一答案
  twoAnsValidators() {
    const paperForm = this.addPaperItem.get('papers') as FormArray;
    paperForm.controls.forEach((item) => {
      const form = item.get('answer') as FormArray;
      form.controls.forEach((child, j) => {
        this.answerSelected[j] = false;
        this.answerUploadSelected[j] = false;
        let answerTextarea = child.get('answer')?.value;
        let answerInput = child.get('answerUpload')?.value;
        child.get('answer')?.setErrors({ isNull: false });
        child.get('answerUpload')?.setErrors({ isNull: false });
        console.log(child);

        if (answerTextarea === '' && answerInput === '') {
          child.get('answer')?.setErrors({ isNull: true });
          child.get('answerUpload')?.setErrors({ isNull: true });
          this.answerSelected[j] = true;
          this.answerUploadSelected[j] = true;
        }

        child.get('answer')?.valueChanges.subscribe((res) => {
          if (res) {
            console.log(res);
            this.answerSelected[j] = false;
            this.answerUploadSelected[j] = false;
          }
        });

        child.get('answerUpload')?.valueChanges.subscribe((res) => {
          if (res) {
            this.answerSelected[j] = false;
            this.answerUploadSelected[j] = false;
          }
        });
      });
    });
  }

  //*二擇一題目
  twoTestValidators() {
    const form = this.addPaperItem.get('papers') as FormArray;
    form.controls.forEach((item, i) => {
      this.testSelected[i] = false;
      this.testPicSelected[i] = false;

      let testTextarea = item.get('test')?.value;
      let testPicInput = item.get('testPic')?.value;
      item.get('test')?.setErrors({ isNull: false });
      item.get('testPic')?.setErrors({ isNull: false });

      if (testTextarea === '' && testPicInput === null) {
        item.get('test')?.setErrors({ isNull: true });
        item.get('testPic')?.setErrors({ isNull: true });
        this.testSelected[i] = true;
        this.testPicSelected[i] = true;
      }

      item.get('test')?.valueChanges.subscribe((res) => {
        if (res) {
          this.testSelected[i] = false;
          this.testPicSelected[i] = false;
        }
      });

      item.get('testPic')?.valueChanges.subscribe((res) => {
        if (res) {
          this.testSelected[i] = false;
          this.testPicSelected[i] = false;
        }
      });
    });
  }
}
