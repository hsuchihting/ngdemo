

import { Component, OnInit } from '@angular/core';

import { AddPaperViewModel } from './add-paper.view-model';
import { VMComponent } from 'src/app/base/vm.component';

@Component({
  selector: 'app-add-paper',
  templateUrl: './add-paper.component.html',
  styleUrls: ['./add-paper.component.scss'],
})
export class AddPaperComponent extends VMComponent<AddPaperViewModel> {
  constructor(
    private item: AddPaperViewModel,

  ) {
    super(item);
  }

  ngOnInit(): void {
    this.vm.init();
  }

  addTestPaper() {
    this.vm.addTestPaper();
  }

  delTestPaper(i: number) {
    this.vm.delTestPaper(i);
  }
  add(i: number) {
    this.vm.add(i);
  }

  del(i: number) {
    this.vm.del(i);
  }

  trackFn(index: any) {
    return index;
  }

  //*驗證所有表單
  saveAllForm() {
    this.ValidateAllFormFields(this.vm.addPaper);
    this.ValidateAllFormFields(this.vm.addPaperItem);
    this.vm.radioConfirm();
    this.vm.checkConfirm();

    // const errorMsg = '資料填寫有誤，再請確認！';
    // if (this.vm.addPaperItem.invalid) {
    //   this.alertService.error(errorMsg);
    // } else {
      this.vm.save();

  }

  onFileChange(event: any, i: number) {
    // this.ValidateAllFormFields(this.vm.addPaperItemChild);
    this.vm.onFileChange(event, i);
  }

  uploadAns(event: any, i: number, j: number) {
    this.vm.uploadAns(event, i, j);
  }

  updateError(event: any, i: number) {
    this.vm.updateError(event, i);
  }

  answerTitle(j: number) {
    return this.vm.answerTitle(j);
  }

  cancel() {
    this.vm.cancel();
  }
}
